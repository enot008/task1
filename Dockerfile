FROM python:3.10
WORKDIR /task1
COPY . ./
CMD ["python", "./task1.py"]
